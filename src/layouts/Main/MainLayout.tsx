import { NavBar } from "./NavBar"


export function MainLayout({ children }: any) {


  return (<>
    <NavBar/>
    <>
      {children}
    </>
    <footer>GEINFO ©2021</footer>
  </>)
}
