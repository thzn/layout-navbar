export interface NavItem {
  label: string;
  subLabel?: string;
  children?: Array<NavItem>;
  href?: string;
}

export const NAV_ITENS: Array<NavItem> = [
  {
    label: 'Início',
    href: '/'
    
  },
  // {
  //   label: 'Sociais',
  //   children: [
  //     {
  //       label: 'Job Board',
  //       subLabel: 'Find your dream design job',
  //       href: '#',
  //     },
  //     {
  //       label: 'Freelance Projects',
  //       subLabel: 'An exclusive list for contract work',
  //       href: '#',
  //     },
  //   ],
  // },
  {
    label: 'Psicologia',
    href: 'produtos',
  },
  {
    label: 'Administrativo',
    href: 'perfil',
  },
  {
    label: 'Holística',
    href: '#',
  },
  {
    label: 'Agenda',
    href: '#',
  },
  {
    label: 'Registro de Atividades',
    href: '#',
  },
  {
    label: 'Relatórios',
    children: [
      {
        label: 'Atividades',
        subLabel: 'Breve descrição',
        href: 'produtos',
      },
      {
        label: 'Declarações',
        subLabel: 'Breve descrição',
        href: '/',
      },
      {
        label: 'Regulares',
        subLabel: 'Breve descrição',
        href: 'produtos',
      },
      {
        label: 'Irregulares',
        subLabel: 'Breve descrição',
        href: 'produtos',
      },
      {
        label: 'Cadastro Inicial',
        subLabel: 'Breve descrição',
        href: 'produtos',
      },
      {
        label: 'Retornos Agendados',
        subLabel: 'Breve descrição',
        href: 'produtos',
      },
    ],
  },
  // {
  //   label: 'Configurações',
  //   children: [
  //     {
  //       label: 'Grupos',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Cidades',
  //       // subLabel: 'Breve descrição',
  //       href: '/',
  //     },
  //     {
  //       label: 'Habilidades',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Cargos / Funções',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Tipos de Atividade',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Tipos de Documento',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Doenças',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Medicamentos',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Partes do Corpo',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Sonhos / Projetos',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Funcionários',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Entidades Parceiras',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //     {
  //       label: 'Unidades de Saúde',
  //       // subLabel: 'Breve descrição',
  //       href: 'produtos',
  //     },
  //   ],
  // },
];