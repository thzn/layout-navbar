import Keycloak from "keycloak-js";

// @ts-ignore
// const keycloakConfig = JSON.parse(process.env.REACT_APP_KEYCLOAK_JSON);

// @ts-ignore
export const keycloak = Keycloak({
  // url: keycloakConfig["auth-server-url"],
  url: 'https://keycloak-dev.sejus.ro.gov.br/auth/',
  // realm: keycloakConfig["realm"],
  realm: 'geinfo',
  // clientId: keycloakConfig["resource"],
  clientId: 'acuda',
});
//login-required | check-sso
// export const keycloakProviderInitConfig: Keycloak.KeycloakInitOptions = {
//   onLoad: "check-sso"
// };