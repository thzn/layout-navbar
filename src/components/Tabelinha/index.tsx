import React from 'react'
import { Button } from '@material-ui/core'
import DataTable from 'react-data-table-component';
import styled from 'styled-components';

const TextField = styled.input`
  height: 32px;
  width: 200px;
  border-radius: 3px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  border: 1px solid #e5e5e5;
  padding: 0 32px 0 16px;

  &:hover {
    cursor: pointer;
  }
`;

const ClearButton = styled(Button)`
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  height: 34px;
  width: 32px;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const FilterComponent = ({ filterText, onFilter, onClear }) => (
  <>
    <TextField id="search" type="text" placeholder="Filter By Name" aria-label="Search Input" value={filterText} onChange={onFilter} />
    <ClearButton type="button" onClick={onClear}>X</ClearButton>
  </>
);

const data = [
  { id: 1, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 2, title: 'Conan the Barbarian',title2: 'Fulano',title3: 'Conan the Barbarian', year: '1982'},
  { id: 3, title: 'Conan the Barbarian',title2: 'CICLANO',title3: 'Conan the Barbarian', year: '1982'},
  { id: 4, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 5, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 6, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 7, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 8, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 9, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 10, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 11, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 12, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},
  { id: 13, title: 'Conan the Barbarian',title2: 'Conan the Barbarian',title3: 'Conan the Barbarian', year: '1982'},

]

export function Tabelinha() {
  const [filterText, setFilterText] = React.useState('');
  const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
  const filteredItems = data.filter(item => item.title && item.title2.toLowerCase().includes(filterText.toLowerCase()));

  const subHeaderComponentMemo = React.useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText('');
      }
    }
    return <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />;
  }, [filterText, resetPaginationToggle]);

  const customStyles = {
    rows: {
      style: {
        minHeight: '30px', // override the row height
      }
    },
    headCells: {
      style: {
        paddingLeft: '8px', // override the cell padding for head cells
        paddingRight: '8px',
      },
    },
    cells: {
      style: {
        paddingLeft: '8px', // override the cell padding for data cells
        paddingRight: '8px',
      },
    },
  };

  const columns = [
    {
      name: 'Nome Coluna 1',
      selector: 'title',
      sortable: true,
    },
    {
      name: 'Nome Coluna 2',
      selector: 'title2',
      sortable: true,
    },
    {
      name: 'Nome Coluna 3',
      selector: 'title3',
      sortable: true,
    },
    {
      name: 'Year',
      selector: 'year',
      sortable: true,
    },
    {
      cell: (data: any) => 
        <>
          <Button variant="contained" color="primary" size="sm">Editar</Button>
          <Button variant="contained" color="secondary" size="sm">Excluir</Button>
        </>
      ,
      button: true,
    },
  ];

  const isBrowser = typeof window !== "undefined"

  return isBrowser ? (
    <DataTable
      title="Nome da tabela"
      columns={columns}
      data={filteredItems}
      pagination
      paginationResetDefaultPage={resetPaginationToggle}
      subHeader
      subHeaderComponent={subHeaderComponentMemo}
      selectableRows
      customStyles={customStyles}
      highlightOnHover     
    />
  ) : null
}