import { AppProps, AppContext } from 'next/app'
import { MainLayout } from '../layouts';
import type { IncomingMessage } from 'http'
import cookie from 'cookie';
import { SSRKeycloakProvider, SSRCookies } from '@react-keycloak/ssr'
import type { Cookies } from  '@react-keycloak/ssr'
import { Provider } from 'react-redux'
import { useStore } from '../store'

interface InitialProps {
  cookies: Cookies
}

const keycloakCfg = {
  realm: 'geinfo',
  url: 'https://keycloak-dev.sejus.ro.gov.br/auth/',
  clientId: 'acuda'
}

function MyApp({ Component, pageProps, cookies }: AppProps & InitialProps) {

  const store = useStore(pageProps.initialReduxState)

  return (
    <>
    <SSRKeycloakProvider keycloakConfig={keycloakCfg} persistor={SSRCookies(cookies)}>
      <Provider store={store}>
        <MainLayout>
          <Component {...pageProps} />
        </MainLayout>
      </Provider>
    </SSRKeycloakProvider>
    </>
  )
}

function parseCookies(req?: IncomingMessage) {
  if (!req || !req.headers) {
    return {}
  }
  return cookie.parse(req.headers.cookie || '')
}
 
// MyApp.getInitialProps = async (context: AppContext) => {
//   // Extract cookies from AppContext
//   return {
//     cookies: parseCookies(context?.ctx?.req)
//   }
// }

export default MyApp
