import { useKeycloak } from '@react-keycloak/ssr'
import type { KeycloakInstance, KeycloakTokenParsed } from 'keycloak-js'

type ParsedToken = KeycloakTokenParsed & {
  email?: string
  preferred_username?: string
  given_name?: string
  family_name?: string
}
 
const Produtos: any = () => {
  const { keycloak } = useKeycloak<KeycloakInstance>()
  const parsedToken: ParsedToken | undefined = keycloak?.tokenParsed

  // console.log(keycloak)

  const loggedinState = keycloak?.authenticated ? (<span className="text-success">Logado</span>)
  : (<span className="text-danger">Deslogado</span>)
 
  const welcomeMessage =
    keycloak?.authenticated || (keycloak && parsedToken)
      ? `Bem vindo de volta ${parsedToken?.preferred_username ?? ''}!`
      : 'Seja Bem vindo Visitante. Efetue o login para continuar.'

  return (
    <div title="Home | Next.js + Keycloak Example">
      <h1 className="mt-5">Next.js + Keycloak 👋</h1>
      <div className="mb-5 lead text-muted">
        Esse é um exemplo de Next.js usando Keycloak.
      </div>
      <p>Você está: {loggedinState}</p>
      <p>{welcomeMessage}</p>
    </div>
  )
}
 
export default Produtos
