import { useKeycloak } from '@react-keycloak/ssr'
import type { KeycloakInstance, KeycloakTokenParsed } from 'keycloak-js'
import type { NextPage } from 'next'
import * as React from 'react'

// import { Layout } from '../components/Layout'

type ParsedToken = KeycloakTokenParsed & {
  email?: string
  preferred_username?: string
  given_name?: string
  family_name?: string
}

const Perfil: NextPage = () => {
  const { keycloak } = useKeycloak<KeycloakInstance>()

  // console.log()
  const parsedToken: ParsedToken | undefined = keycloak?.tokenParsed

  const profile = keycloak?.authenticated ? (
    <>
    <ul>
      <li>
        <span className="font-weight-bold mr-1">Email: </span>
        <span className="text-muted">{parsedToken?.email ?? ''}</span>
      </li>
      <li>
        <span className="font-weight-bold mr-1">Username: </span>
        <span className="text-muted">
          {parsedToken?.preferred_username ?? ''}
        </span>
      </li>
      <li>
        <span className="font-weight-bold mr-1">Nome: </span>
        <span className="text-muted">{parsedToken?.given_name ?? ''}</span>
      </li>
      <li>
        <span className="font-weight-bold mr-1">Sobrenome: </span>
        <span className="text-muted">{parsedToken?.family_name ?? ''}</span>
      </li>
    </ul>
    <button onClick={() => {keycloak?.logout()}}>LOGOUT</button>
    </>
  ) : (
    <div>
      <p>Efetue o login para visualisar o seu perfil.</p>
      <button onClick={() => {keycloak?.login()}}>LOGIN</button>
    </div>
  )

  return (
    <div title="Profile | Next.js + Keycloak Example">
      <h1 className="my-5">Perfil do usuário:</h1>
      {profile}
    </div>
  )
}

export default Perfil